import requests_async as req, json

class LongPoll:
    ## Private:
    async def _poll(self):
        response = (await req.get(self._url)).json()
        if 'ts' in response:
            self.change_ts(response['ts'])
        if 'failed' in response:
            return [response]
        return response['updates']
    ## Public:
    def __init__(self):
        pass

    async def __call__(self, cfg):
        self._cfg = cfg
        self._url = '{s}?act=a_check&key={k}&ts={t}&wait={w}'.format(s=self._cfg['server'], k=self._cfg['key'], t=self._cfg['ts'], w=self._cfg['wait'])
        while True:
            for i in (await self._poll()):
                yield i

    def change_ts(self, ts):
        self._cfg['ts'] = ts
        self._url = '{s}?act=a_check&key={k}&ts={t}&wait={w}'.format(s=self._cfg['server'], k=self._cfg['key'], t=self._cfg['ts'], w=self._cfg['wait'])
