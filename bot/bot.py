import json
from bot.longpoll_server import LongPoll as LP
from bot.vk_request import VKRequest as VKReq

class Bot:
    ## Private:
    def _get_longpoll(self):  # Key, server, ts, wait
        data = self._req('groups.getLongPollServer', group_id=self._cfg['group_id'])
        return {'key': data['key'], 'server': data['server'], 'ts': data['ts'], 'wait': self._cfg['wait']}
    ## Public:
    def __init__(self, file, handler):
        self._status_msg = {}
        self._cfg_file = file
        with open(file, 'r') as f:
            f.seek(0)
            self._cfg = json.load(f)['config']
            self._longpoll = LP()
            self._req = VKReq(self._cfg['access_token'], self._cfg['vk_v'])
            self._handler = handler(self)

    def status(self):
        return self._status_msg

    async def start(self):
        yield 'Starting bot...'
        self._status_msg['is_running'] = True
        while self._status_msg['is_running']:
            async for event in self._longpoll(self._get_longpoll()):
                if not self._status_msg['is_running']:
                    break
                if 'failed' in event:
                    if event['failed'] != 1:
                        break
                else:
                    for i in self._handler(event):
                        yield i
        yield 'Bot stopped.'

    def stop(self):
        if 'is_running' in self._status_msg and self._status_msg['is_running']:
            self._status_msg['is_running'] = False
            self._status_msg['cause'] = 'Выключение по команде.'
            with open(self._cfg_file, 'w') as f:
                json.dump({'config': self._cfg}, f, indent=4)
                return 'Cfg saved'
