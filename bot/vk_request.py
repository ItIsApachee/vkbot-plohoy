import requests as req, json
_vk_api_prefix = 'https://api.vk.com/method/'
class VKRequest:

    def __init__(self, access_token, version):
        self._access_token, self._version = access_token, version

    def __call__(self, method, **kwargs):
        response = {}
        try:
            response = req.post(_vk_api_prefix + method + '?access_token={}&v={}'.format(self._access_token, self._version), data=kwargs).json()
            return response['response']
        except Exception as e:
            return e
            #print('Ошибка! Ответ сервера: ', e)
        return response
