from tokenizer.exception import TokenException

_tag_chars = "abcdefghijklmnopqrstuvwxyz0123456789_"
_spec_chars = {
    "\"": (0, lambda a: "\""),
    "'": (0, lambda a: "'"),
    "\\": (0, lambda a: "\\"),
    "n": (0, lambda a: "\n"),
    "a": (0, lambda a: ""),
    "b": (0, lambda a: ""),
    "f": (0, lambda a: ""),
    "r": (0, lambda a: ""),
    "t": (0, lambda a: ""),
    "v": (0, lambda a: ""),
    "u": (4, lambda a: eval("'\\u" + a + "'")),
    "U": (8, lambda a: eval("'\\U" + a + "'")),
    "x": (2, lambda a: eval("'\\x" + a + "'")),
    "0": (0, lambda a: "")
}
_token_types = [
    "text",
    "tag"
]

def _get_spec_ch(s):
    if s == []:
        raise TokenException("wrong special character")
    sp_ch = s.pop(0) ##
    l, f = _spec_chars[sp_ch]
    if len(s) < l:
        raise TokenException("wrong special character format")
    c = f(s[0:l]) ##
    for i in range(l): ##
        s.pop(0) ##
    return c

def _get_word(char, s):
    buf = char
    while s != [] and s[0] not in [" ", "\n"]: ##
        char = s.pop(0) ##
        if char == "\\":
            char = _get_spec_ch(s)
        buf += char
    return (_token_types[0], buf.lower())
def _get_text(s, t):
    buf = ""
    while s != []:
        char = s.pop(0) ##
        if char == t:
            if buf == "":
                raise TokenException("wrong format of string in quotes, empty quotes")
            return [(_token_types[0], buf.strip().lower())] # Successfull return
        elif char == "\\":
            char = _get_spec_ch(s)
        buf += char
    raise TokenException("wrong format of string in quotes, unclosed quotes")
def _get_tag(s, t):
    buf1, buf2 = "", ""
    while s != [] and s[0] in _tag_chars: ##
        buf1 += s.pop(0) ##
    i = -1
    while len(s) > i + 1 and s[i+1] == " ":
        i += 1
    if (t == "[" and (len(s) <= 2 or s[0] != "|")) or (t != "[" and s != [] and s[0] not in ["(", " "]): ##
        while s != [] and s[0] != " ": ##
            char = s.pop(0) ##
            if char == "\\":
                char = _get_spec_ch(s)
            buf1 += char
        return [(_token_types[0], buf1)]
    elif (t != "[" and (s == [] or s[i+1] != "(")): ##
        return [(_token_types[1], buf1.lower(), (t + buf1).lower())]
    else:
        if t != "[":
            arr = s[i+2:] ##
        else:
            arr = s[1:] ##
        temp, buf2 = _get_text(arr, "]" if t == "[" else ")")[0] ##
        for i in range(i+3+len(buf2)): ##
            s.pop(0) ##
        return [(_token_types[1], buf1.lower(), buf2.lower())]



_token_funcs = {
    '"': lambda a: _get_text(a, '"'),
    "'": lambda a: _get_text(a, "'"),
    "@": lambda a: _get_tag(a, "@"),
    "*": lambda a: _get_tag(a, "*"),
    "[": lambda a: _get_tag(a, "["),
}

def split(s):
    """
    Разделяет строку s на список токенов.

    @param s Входная строка.
    @return Список list, состоящий из токенов.
    """
    s = list(s)
    while s != []:
        char = s.pop(0) ##
        if char == "\\":
            char = _get_spec_ch(s)
            yield _get_word(char, s)
        elif char in _token_funcs:
            for i in _token_funcs[char](s):
                yield i
        elif char in [" ", "\n"]:
            continue
        else:
            yield _get_word(char, s)
