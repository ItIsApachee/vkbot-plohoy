import random

random.seed()

from handler.handler import *
from handler.message import *
from handler.chat import *
from handler.user import *
