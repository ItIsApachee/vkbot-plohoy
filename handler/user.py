import json

from handler.handler import Handler

class User:
    def __init__(self, **kwargs):
        self._fields = kwargs
    def __getitem__(self, key: str):
        if key in self._fields:
            return self._fields[key]
        raise KeyError('\'' + key + '\' not in user fields')
    def __str__(self):
        return json.dumps(self._fields)
    def get_tag(self):
        if 'screen_name' in self._fields:
            screen_name = self['screen_name']
        else:
            screen_name = 'id' + str(self['id'])
        return '[id' + str(self['id']) + '|@' + screen_name + ']'

class UsersHandler:
    ## Public:
    def __init__(self, handler: Handler):
        self._handler = handler
        self._req = handler._req

    def get_single(self, user_id, fields = []):
        s_fields = ''
        for id, field in enumerate(fields):
            s_fields += field
            if id != len(fields)-1:
                s_fields += ','
        return User(**(self._req('users.get', user_ids=str(user_id), fields=s_fields).pop()))
    def __call__(self, ids, fields = []):
        ret = {}
        s_fields = ''
        s_ids = ''
        for id, field in enumerate(fields):
            s_fields += field
            if id != len(fields)-1:
                s_fields += ','
        for id, key in enumerate(ids):
            s_ids += str(key)
            if id != len(ids)-1:
                s_ids += ','
        for i in self._req('users.get', user_ids=s_ids, fields=s_fields):
            ret[i['id']] = User(**i)
        return ret
