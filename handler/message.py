import random, json, io

from bot import Bot

class Message:
    pass

class MessageHandler:
    pass

from handler.handler import Handler
from handler.command import CommandHandler

class Message:
    ## Public:
    def __init__(self, msg_handler: MessageHandler, msg={}, message_flags={}):
        self._req = msg_handler._req
        self._msg = msg
        self._flags = message_flags

    def __str__(self):
        return json.dumps({
            'msg': self._msg,
            'flags': self._flags
        }, indent=4)

    def __getitem__(self, key: str):
        if key in self._msg:
            return self._msg[key]
        raise KeyError('\'' + key + '\' not in msg')

    def flag(self, key: str):
        if key in self._flags:
            return self._flags[key]
        raise KeyError('\'' + key + '\' not in flags')

    def send(self):
        return str(
            self._req(
                'messages.send',
                random_id=random.randint(-2147483640, 2147483640),
                **self._msg
            )
        )

class MessageHandler:
    ## Private:

    def _send_error(self, msg: Message, text: str, additional_data: dict = {}):
        prefix = '[error]: ' if self._cfg['debug'] else ''
        return self._print('Response to client (fail): ', self.send_msg(msg={
            **{key: msg[key] for key in ('peer_id',)},
            'message': prefix + text,
            **additional_data
        }), '.', sep='')

    def _send_message(self, msg: Message, text:str, additional_data: dict = {}):
        prefix = '[success]: ' if self._cfg['debug'] else ''
        return self._print('Response to client (success): ', self.send_msg(msg={
            **{key: msg[key] for key in ('peer_id',)},
            'message': prefix + text,
            **additional_data
        }), '.', sep='')

    def _print(self, *args, **kwargs):
        out = io.StringIO()
        print(*args, **kwargs, end='', file=out)
        return out.getvalue()

    def _handle_text(self, msg: Message):
        if msg.flag('is_cmd'):
            for i in self._handler._cmd_handler(msg):
                yield i
        elif not msg.flag('is_chat'):
            yield self._send_error(msg, 'Unsupported operation.')
        else:
            return
    def _handle_rest_msg(self, msg: Message):
        if msg.flag('is_action'):
            for i in self._handle_action(msg):
                yield i
        elif not msg.flag('is_chat'):
            yield self._send_error(msg, 'Unsupported operation.')
        else:
            return
    def _handle_action(self, msg: Message):
        yield self._send_error(msg, 'Unsupported operation (action).')

    ## Public:
    def __init__(self, handler: Handler):
        self._handler = handler
        self._req = handler._req
        self._cfg = handler._cfg
        #self._cmd_handler = CommandHandler(handler)

    def new(self, message_flags={}, msg={}):
        return Message(self, msg=msg, message_flags=message_flags)

    def send_msg(self, msg={}):
        return Message(self, msg=msg).send()

    def __call__(self, msg):
        msg_flags = {
            'is_from_bot': msg['from_id'] < 0,
            'is_cmd': self._handler._cmd_handler.check(msg['text']),
            'is_chat': msg['peer_id'] > 2000000000,
            'is_empty': not msg['text'],
            'is_action': 'action' in msg,
            'is_reply': 'reply_message' in msg,
            'have_attachment': bool(msg['attachments']),
            'have_fwd_messages': bool(msg['fwd_messages']),
        }
        msg = self.new(message_flags=msg_flags, msg=msg)
        if msg.flag('is_empty'):
            for i in self._handle_rest_msg(msg):
                yield i
        else:
            for i in self._handle_text(msg):
                yield i
