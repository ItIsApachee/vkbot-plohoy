import json

from bot import Bot

# class Handler:
#     def __init__(self, bot: Bot):
#         pass
#     def __call__(self, event):
#         pass

class Handler:
    ## Private:
    ## Public:
    def __init__(self, bot: Bot):
        self._bot = bot
        self._cfg = bot._cfg
        self._req = bot._req
        self._cmd_handler = CommandHandler(self)
        self._msg_handler = MessageHandler(self)
        self._users_handler = UsersHandler(self)
        self._chat_handler = ChatHandler(self)

    def __call__(self, event):
        if event['type'] == 'message_new':
            for i in self._msg_handler(event['object']):
                yield i
        return

from handler.message import MessageHandler
from handler.command import CommandHandler
from handler.user import UsersHandler
from handler.chat import ChatHandler
