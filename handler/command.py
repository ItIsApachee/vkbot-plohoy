from tokenizer import tokenize
from handler.handler import Handler
from handler.message import Message, MessageHandler

'''
    To do:
        - permission management,
        - mailing management,
        - targets management,
        - chat get users and more,
        - ping everyone
'''

class Command:
    ## Private:
    def _perm_test(self, id):
        if self.permission < 0:
            return True
        else:
            return True if str(id) in self._cfg['permissions'] and self._cfg['permissions'][str(id)] <= self.permission else False

    ## Public:
    def __init__(self, handler: Handler, cmd:str, permission: int, cmd_handler):
        self._handler = handler
        self._cfg = handler._cfg
        self.cmd_name = cmd
        self.permission = permission
        self.cmd_handler = cmd_handler

    def __call__(self, msg: Message, tokens: list):
        if self._perm_test(msg['from_id']):
            for i in self.cmd_handler(msg, tokens):
                yield i
        else:
            yield self._handler._msg_handler._send_error(msg, 'Недостаточно полномочий.')


class CommandHandler:
    ## Public:
    def __init__(self, handler: Handler):
        self._handler = handler
        self._cfg = handler._cfg
        self._commands = [
            Command(handler, 'test', -1, self._test_command),
            Command(handler, 'тест', -1, self._test_command),
            Command(handler, 'perm_test', -1, self._perm_test_command),
            Command(handler, 'проверка_доступа', -1, self._perm_test_command),
            Command(handler, 'debug', 1, self._debug_command), ## Сделать дебаг без параметров для того, чтобы узнать значение
            Command(handler, 'stop', 0, self._stop_command),
            Command(handler, 'остановить', 0, self._stop_command),
            Command(handler, 'help', -1, self._help_command), ## Сделать помощь по каждой команде
            Command(handler, 'помощь', -1, self._help_command),
            Command(handler, 'send_to_target', 1, self._send_to_target_command), ## Сделать возможность делать рассылку без текста
            Command(handler, 'members', 0, self._members_command),
            Command(handler, 'ping', 0, self._ping_command)
        ]

    def check(self, text: str):
        p = self._cfg['prefix']
        return len(text) >= len(p) and text[0:len(p)] == p

    def __call__(self, msg: Message):
        tokens = [i for i in tokenize(msg['text'])]
        command = tokens[0][1][len(self._handler._cfg['prefix']):]
        cmd_f = next(filter(lambda x: x.cmd_name == command, self._commands), None)
        if cmd_f == None:
            yield self._handler._msg_handler._send_error(msg, 'Несуществующая команда!')
            return
        for i in cmd_f(msg, tokens):
            yield i

    ## Private:
    def _get_permission(self, id):
        if str(id) in self._cfg['permissions']:
            return self._cfg['permissions'][str(id)]
        else:
            return -1

    def _test_command(self, msg: Message, tokens: list):
        yield self._handler._msg_handler._send_message(msg, 'Тест прошел удачно!')

    def _perm_test_command(self, msg: Message, tokens: list):
        if len(tokens) == 1:
            usr = self._handler._users_handler.get_single(msg['from_id'], ['screen_name'])
        elif len(tokens) == 2:
            if tokens[1][0] != 'text':
                try:
                    usr = self._handler._users_handler.get_single(tokens[1][1], ['screen_name'])
                except:
                    yield self._handler._msg_handler._send_error(msg, 'Неверный тэг!')
                    return
            else:
                yield self._handler._msg_handler._send_error(msg, 'Ожидался тэг, не строка.')
                return
        else:
            yield self._handler._msg_handler._send_error(msg, 'Неверный синтаксис команды!')
            return
        yield self._handler._msg_handler._send_message(msg, self._handler._msg_handler._print('Право доступа ', usr.get_tag(), ' равно ', self._get_permission(usr['id']), '.', sep=''))

    def _debug_command(self, msg: Message, tokens: list):
        if len(tokens) == 2:
            if tokens[1][1] in ('true', 'false'):
                self._cfg['debug'] = True if tokens[1][1] == 'true' else False
                yield self._handler._msg_handler._send_message(msg, self._handler._msg_handler._print('config.json: debug = ', str(self._cfg['debug']).lower(), '.', sep=''))
            else:
                yield self._handler._msg_handler._send_error(msg, 'Неверный 2ой аргумент!')
        else:
            yield self._handler._msg_handler._send_error(msg, 'Неверный синтаксис команды!')

    def _stop_command(self, msg: Message, tokens: list):
        yield self._handler._msg_handler._send_message(msg, 'Останавливаем...')
        yield self._handler._bot.stop()
        yield self._handler._msg_handler._send_message(msg, 'Остановлено.')

    def _help_command(self, msg: Message, tokens: list):
        if len(tokens) == 1:
            text = 'Список команд: '
            for id, key in enumerate(self._commands):
                text += key.cmd_name
                if len(self._commands)-1 > id:
                    text += ', '
                else:
                    text += '.'
        else:
            yield self._handler._msg_handler._send_error(msg, 'Неверный синтаксис команды!')
            return
        yield self._handler._msg_handler._send_message(msg, text)

    def _send_to_target_command(self, msg: Message, tokens: list):
        if len(tokens) == 3:
            if tokens[1][0] == 'text' and tokens[1][1] in self._cfg['targets']:
                text = tokens[2][1] if tokens[2][0] == 'text' else ('[' + tokens[2][1] + '|' + tokens[2][2] + ']')
                yield 'Mailing ' + str(msg)
                forward_messages = ''
                for id, key in enumerate(msg['fwd_messages']):
                    forward_messages += str(msg['fwd_messages'][id]['id'])
                    if len(msg['fwd_messages'])-1 > id:
                        forward_messages += ','
                attaches = ''
                for id, key in enumerate(msg['attachments']):
                    att = msg['attachments'][id]
                    attaches += att['type'] + str(att[att['type']]['owner_id']) + '_' + str(att[att['type']]['id'])
                    if len(msg['attachments'])-1 > id:
                        attaches += ','
                for i in self._cfg['targets'][tokens[1][1]]:
                    self._handler._msg_handler.send_msg({
                        'peer_id': i,
                        'message': text,
                        'forward_messages': forward_messages,
                        'attachment': attaches
                    })
                yield 'Mailed successfully.'
            else:
                yield self._handler._msg_handler._send_error(msg, 'Неверный 2ой аргумент!')
                return
        else:
            yield self._handler._msg_handler._send_error(msg, 'Неверный синтаксис команды!')
            return

    def _members_command(self, msg: Message, tokens: list):
        if len(tokens) == 1:
            yield str(self._handler._chat_handler(msg['peer_id']))
        else:
            yield self._handler._msg_handler._send_error(msg, 'Неверный синтаксис команды!')
            return

    def _ping_command(self, msg: Message, tokens: list):
        if len(tokens) == 1:
            yield self._handler._msg_handler._send_message(msg, ''.join(i.get_tag() + ', ' for i in self._handler._chat_handler(msg['peer_id'], ['screen_name']).get_members())[:-2])
        else:
            yield self._handler._msg_handler._send_error(msg, 'Неверный синтаксис команды!')
            return
