import json

from handler.handler import Handler
from handler.user import User

class Chat:
    def __init__(self, members: list = []):
        self._members = []
        for i in members:
            self._members.append(User(**i))

    def __str__(self):
        return json.dumps([str(i) for i in self._members], indent=4)

    def get_members(self):
        for i in range(len(self._members)):
            yield self._members[i]

class ChatHandler:
    ## Public:
    def __init__(self, handler: Handler):
        self._handler = handler
        self._req = handler._req
    def __call__(self, peer_id, fields = []):
        s_fields = ''
        for id, key in enumerate(fields):
            s_fields += key
            if len(fields)-1 > id:
                s_fields += ','
        try:
            return Chat(self._req('messages.getConversationMembers', peer_id=peer_id, fields=s_fields)['profiles'])
        except:
            return Chat()
