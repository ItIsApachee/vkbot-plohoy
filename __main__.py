"""
    Using:
        asyncio, requests, requests-async, (curses)
    TO DO:
        - FIX TOKENIZER! (list.pop(0))
"""
import time, asyncio, sys
from concurrent.futures import FIRST_COMPLETED, ThreadPoolExecutor

from bot import Bot
from handler import Handler

class BotConsole:
    def _debug(self, *args):
        print(time.strftime('[%H:%M:%S]'), *args)

    def __init__(self, cfg: str, ioloop):
        self._bot = Bot(cfg, Handler(cfg))
        self._is_running = False
        self._ioloop = ioloop

    async def _bot_wrap(self):
        async for i in self._bot.start():
            self._debug(i)
        self._bot.stop()

    async def _console_output(self):
        while self._is_running:
            await asyncio.sleep(5)

    async def run(self):

        self._is_running = True
        tasks = [asyncio.ensure_future(self._bot_wrap()), asyncio.ensure_future(self._console_output())]

        done, pending = await asyncio.wait(tasks, return_when=FIRST_COMPLETED)

        self._bot.stop()
        for futures in pending:
            futures.cancel()
        self._is_running = False

    def status(self):
        return self._bot.status()

if __name__ == '__main__':
    cfg = 'config.json'

    ioloop = asyncio.get_event_loop()
    console = BotConsole(cfg, ioloop)
    ioloop.run_until_complete(console.run())
    ioloop.close()

    print('Бот завершил свою работу со статусом:', console.status())
